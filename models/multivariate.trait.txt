model{
  
  # Model structure

  for(i in 1:n_obvs){
    Y[i, 1:n_traits] ~ dmnorm(mu_trait, invSigma_trait)
    for(j in 1:n_traits){
      obvs[i,j] ~ dnorm(Y[i,j], 10000)
    }
  }
  
  # Priors
  
  mu_trait[1:n_traits]~dmnorm(mu0[], Sigma0[,])
  invSigma_trait ~ dwish(Wishart.rate, Wishart.df)
  Sigma_trait <- inverse(invSigma_trait)
  
}
